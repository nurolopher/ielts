package com.nurolopher.ieltswords;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.nurolopher.ieltswords.model.Statistics;
import com.nurolopher.ieltswords.model.Word;
import com.nurolopher.ieltswords.utils.Utils;

import java.util.ArrayList;
import java.util.Random;

public class QuizActivity extends AppCompatActivity {

    private Statistics statistics;
    private ArrayList<Word> allWords;
    private int currentIndex;
    private final int CHOICE_COUNT = 4;
    private Integer[] questionIndexes;
    private Integer[] choiceIndexes;
    private TextView textViewDefinition;
    private RadioGroup radioGroup;
    private RadioButton[] radioButtons;
    private int answerIndex;
    private Random random;
    private Word answer;
    private ImageButton btnNext;
    private TextView textViewCurrentIndex;
    private TextView textViewAnswer;
    private int questionCount;
    private boolean clearChecked;
    private Button statisticsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        setupActionBar();
        initPreferences();
        init();
        initWidgets();
        registerListeners();
        showQuestion();
    }

    private void showQuestion() {

        if (currentIndex >= questionCount) {
            return;
        }
        answerIndex = random.nextInt(CHOICE_COUNT);
        choiceIndexes = Utils.getRandomHashSet(CHOICE_COUNT, allWords.size(), questionIndexes[currentIndex]);
        for (int index = 0; index < CHOICE_COUNT; index++) {
            radioButtons[index].setText(allWords.get(choiceIndexes[index]).getTitle());
        }
        answer = allWords.get(questionIndexes[currentIndex]);
        radioButtons[answerIndex].setText(answer.getTitle());
        textViewDefinition.setText(answer.getDefinition());

        updateCurrentIndexStatus();


        clearCheck();
        setEnabledRadioButtons(true);
        setEnabledBtnNext(false);

        //Answer status is empty at first!
        textViewAnswer.setVisibility(View.INVISIBLE);
    }

    private void clearCheck() {
        clearChecked = true;
        radioGroup.clearCheck();
        clearChecked = false;
    }

    private void updateCurrentIndexStatus() {
        currentIndex++;
        textViewCurrentIndex.setText(String.format("%d / %d", currentIndex, questionCount));
    }

    private void registerListeners() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId <= -1 || clearChecked) {
                    return;
                }
                if (checkedId == radioButtons[answerIndex].getId()) {
                    statistics.increaseRight();
                    updateAnswerStatus(true);
                } else if (containsId(checkedId)) {
                    statistics.increaseWrong();
                    updateAnswerStatus(false);
                }
                setEnabledRadioButtons(false);
                setEnabledBtnNext(true);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentIndex < questionCount) {
                    showQuestion();
                } else {
                    showLastMessage();
                    statistics.save();
                    statisticsButton.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private boolean containsId(int checkId) {
        for (RadioButton button : radioButtons) {
            if (button.getId() == checkId && button.isChecked()) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void showLastMessage() {
        radioGroup.setVisibility(View.GONE);
        textViewAnswer.setVisibility(View.GONE);
        btnNext.setVisibility(View.GONE);

        String format = getResources().getString(R.string.quiz_last_message);

        //Dirty and quick hack!
        if (statistics.getRight() < 2) {
            format = format.replace("are correct", "is correct");
        }
        String message = String.format(format, statistics.getRight(), this.questionCount, statistics.rightInPercent());
        textViewDefinition.setText(message);
    }

    private void updateAnswerStatus(boolean right) {
        String status;
        int color;
        if (right) {
            status = getResources().getString(R.string.correct);
            color = ContextCompat.getColor(this, R.color.green);

        } else {
            status = getResources().getString(R.string.wrong);
            color = ContextCompat.getColor(this, R.color.red);
        }
        textViewAnswer.setText(status);
        textViewAnswer.setBackgroundColor(color);
        textViewAnswer.setVisibility(View.VISIBLE);
    }

    private void setEnabledRadioButtons(boolean enabled) {
        for (int i = 0, len = radioGroup.getChildCount(); i < len; i++) {
            radioGroup.getChildAt(i).setEnabled(enabled);
        }
    }

    private void setEnabledBtnNext(boolean enabled) {
        btnNext.setEnabled(enabled);
        btnNext.setClickable(enabled);
    }

    private void initWidgets() {
        textViewDefinition = (TextView) findViewById(R.id.textViewDefinition);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        radioButtons[0] = (RadioButton) findViewById(R.id.radioButtonA);
        radioButtons[1] = (RadioButton) findViewById(R.id.radioButtonB);
        radioButtons[2] = (RadioButton) findViewById(R.id.radioButtonC);
        radioButtons[3] = (RadioButton) findViewById(R.id.radioButtonD);

        btnNext = (ImageButton) findViewById(R.id.btn_next);

        textViewCurrentIndex = (TextView) findViewById(R.id.textViewCurrentIndex);
        textViewAnswer = (TextView) findViewById(R.id.textViewAnswer);

        statisticsButton = (Button) findViewById(R.id.statistics);
        statisticsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), StatisticsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void init() {
        statistics = new Statistics();
        allWords = (ArrayList<Word>) Word.listAll(Word.class);
        questionIndexes = Utils.getRandomHashSet(questionCount, allWords.size());
        radioButtons = new RadioButton[4];
        random = new Random();
        currentIndex = 0;
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String perGroupDefault = getResources().getString(R.string.question_number_default);
        questionCount = Integer.parseInt(sharedPreferences.getString("question_number", perGroupDefault));
    }

}