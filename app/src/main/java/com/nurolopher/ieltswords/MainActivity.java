package com.nurolopher.ieltswords;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.nurolopher.ieltswords.model.Word;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    private static final String LAUNCHED = "LAUNCHED";
    private View wordList;
    private View exercise;
    private View quiz;
    private View statistics;
    private View settings;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        registerListeners();
        loadFixtures();
    }

    private void loadFixtures() {
        if (!isLaunched()) {
            new LoadFixturesTask().execute("1.sql");
        }
    }

    private boolean isLaunched() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        boolean launched = sharedPref.getBoolean(MainActivity.LAUNCHED, false);
        if (!launched) {
            editor.putBoolean(MainActivity.LAUNCHED, true);
            editor.apply();
        }
        return launched;
    }

    private class LoadFixturesTask extends AsyncTask<String, Integer, Long> {

        @Override
        protected Long doInBackground(String... params) {
            try {
                InputStream is = getAssets().open("fixtures/" + params[0]);
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line;
                int index = 0;
                while ((line = reader.readLine()) != null) {
                    Log.i("Sugar script", line);
                    Word.executeQuery(line);
                    publishProgress(index);
                    index++;
                }
            } catch (IOException e) {
                Log.e("Sugar", e.getMessage());
            }

            Log.i("Sugar", "script executed");
            return 1L;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            Snackbar.make(findViewById(R.id.root), R.string.preparing, Snackbar.LENGTH_LONG).show();
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            progressBar.setVisibility(View.GONE);
            Snackbar.make(findViewById(R.id.root), R.string.done, Snackbar.LENGTH_LONG).show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
        }
    }


    private void registerListeners() {
        wordList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WordListActivity.class);
                startActivity(intent);
            }
        });

        exercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WordGroupActivity.class);
                startActivity(intent);
            }
        });

        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
                startActivity(intent);
            }
        });
        statistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), StatisticsActivity.class);
                startActivity(intent);
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void init() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        wordList = findViewById(R.id.wordList);
        exercise = findViewById(R.id.exercise);
        quiz = findViewById(R.id.quiz);
        statistics = findViewById(R.id.statistics);
        settings = findViewById(R.id.settings);
    }
}
