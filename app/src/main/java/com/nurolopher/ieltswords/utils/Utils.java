package com.nurolopher.ieltswords.utils;

import java.util.HashSet;
import java.util.Random;

/**
 * Created by nurolopher on 11/8/2015.
 */
public class Utils {
    public static int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            throw new IllegalArgumentException
                    (l + " cannot be cast to int without changing its value.");
        }
        return (int) l;
    }

    public static Integer[] getRandomHashSet(int size, int maxNumber) {
        return getRandomHashSet(size, maxNumber, -1);
    }

    public static Integer[] getRandomHashSet(int size, int maxNumber, int except) {
        Random random = new Random();
        int num;
        HashSet<Integer> hashSet = new HashSet<>();
        while (hashSet.size() < size) {
            num = random.nextInt(maxNumber);
            if (num != except)
                hashSet.add(num);
        }
        return hashSet.toArray(new Integer[size]);
    }
}
