package com.nurolopher.ieltswords;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.nurolopher.ieltswords.adapter.WordListAdapter;
import com.nurolopher.ieltswords.model.Word;

import java.util.List;

public class WordListActivity extends AppCompatActivity {

    List<Word> wordList;
    private WordListAdapter mAdapter;
    String memorized;
    private String restored;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_list);
        memorized = getResources().getString(R.string.memorized);
        restored = getResources().getString(R.string.restored);
        setupActionBar();
        configListView();
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void configListView() {
        wordList = Word.findWithQuery(Word.class, "SELECT * FROM WORD WHERE STATUS = " + Word.NEW);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.worldListListView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new WordListAdapter(this, wordList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.word_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_exercise) {
            Intent intent = new Intent(this, ExerciseActivity.class);
            startActivity(intent);
        } else if (id == R.id.action_show_all) {
            mAdapter.filter(Word.ALL);
            changeTitle(R.string.words_all);
        } else if (id == R.id.action_show_new) {
            mAdapter.filter(Word.NEW);
            changeTitle(R.string.words_new);
        } else if (id == R.id.action_memorized) {
            mAdapter.filter(Word.MEMORIZED);
            changeTitle(R.string.action_memorized);
        }

        return super.onOptionsItemSelected(item);
    }

    private void changeTitle(int resourceId) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getResources().getString(resourceId));
        }
    }

    public void showSnackBar(final Word word, final int position) {
        String text = (word.getStatus() == Word.MEMORIZED) ? memorized : restored;
        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), text, Snackbar.LENGTH_LONG);

        snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
        snackbar.setActionTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));

        if (word.getStatus() == Word.MEMORIZED) {
            snackbar.setAction("RESTORE", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    word.setStatus(Word.NEW);
                    word.save();
                    wordList.add(position, word);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
        snackbar.show();
    }
}
