package com.nurolopher.ieltswords.filter;

import android.widget.Filter;

import com.nurolopher.ieltswords.adapter.WordListAdapter;
import com.nurolopher.ieltswords.model.Word;

import java.util.ArrayList;

/**
 * Created by nurolopher on 11/10/2015.
 */
public class WordFilter extends Filter {

    private ArrayList<Word> wordArrayList;
    private WordListAdapter adapter;

    public WordFilter(ArrayList<Word> wordArrayList, WordListAdapter adapter) {
        this.wordArrayList = wordArrayList;
        this.adapter = adapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults filterResults = new FilterResults();
        ArrayList<Word> wordList = new ArrayList<>();
        int status = Integer.parseInt(constraint.toString());
        if (status == Word.ALL) {
            filterResults.values = wordArrayList;
            filterResults.count = wordArrayList.size();
        } else {
            for (Word word : wordArrayList) {
                if (word.getStatus() == status) {
                    wordList.add(word);
                }
            }
            filterResults.values = wordList;
            filterResults.count = wordList.size();
        }
        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
//        if (results.count == 0) {
//            adapter.notifyDataSetInvalidated();
//        } else {
//            adapter.clear();
//            adapter.addAll((ArrayList<Word>) results.values);
//            adapter.notifyDataSetChanged();
//        }
    }

}
