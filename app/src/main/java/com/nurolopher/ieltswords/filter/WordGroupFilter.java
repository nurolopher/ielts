package com.nurolopher.ieltswords.filter;

import android.widget.Filter;

import com.nurolopher.ieltswords.adapter.WordGroupAdapter;
import com.nurolopher.ieltswords.model.WordGroup;

import java.util.ArrayList;

/**
 * Created by nurolopher on 11/10/2015.
 */
public class WordGroupFilter extends Filter {

    private final int perGroup;
    private ArrayList<WordGroup> wordGroupArrayList;
    private WordGroupAdapter adapter;

    public WordGroupFilter(ArrayList<WordGroup> wordGroupArrayList, int perGroup, WordGroupAdapter adapter) {
        this.wordGroupArrayList = wordGroupArrayList;
        this.adapter = adapter;
        this.perGroup = perGroup;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults filterResults = new FilterResults();
        ArrayList<WordGroup> wordGroupList = new ArrayList<>();
        String filter = constraint.toString();
        if (filter.equals(WordGroup.ALL)) {
            filterResults.values = wordGroupArrayList;
            filterResults.count = wordGroupArrayList.size();
        } else {
            for (WordGroup wordGroup : wordGroupArrayList) {
                if (wordGroup.getNumberOfMemorized() < perGroup) {
                    wordGroupList.add(wordGroup);
                }
            }
            filterResults.values = wordGroupList;
            filterResults.count = wordGroupList.size();
        }
        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
//        if (results.count == 0) {
//            adapter.notifyDataSetInvalidated();
//        } else {
//            adapter.clear();
//            adapter.addAll((ArrayList<WordGroup>) results.values);
//            adapter.notifyDataSetChanged();
//        }
    }

}
