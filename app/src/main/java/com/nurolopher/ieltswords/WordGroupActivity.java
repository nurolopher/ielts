package com.nurolopher.ieltswords;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.nurolopher.ieltswords.adapter.WordGroupAdapter;
import com.nurolopher.ieltswords.model.Word;
import com.nurolopher.ieltswords.model.WordGroup;

import java.util.ArrayList;
import java.util.List;

public class WordGroupActivity extends AppCompatActivity {


    private List<WordGroup> wordGroupList;
    private List<WordGroup> wordGroupListAll;
    private int perGroup;
    protected List<Word> wordList;
    private RecyclerView mRecyclerView;
    private WordGroupAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_group);
        setupActionBar();
        init();
        initPreferences();
        initWordGroupData();
        configListView();
    }

    private void init() {
        wordList = Word.listAll(Word.class);
        wordGroupList = new ArrayList<>();
        wordGroupListAll = new ArrayList<>();
    }

    private void initPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String perGroupDefault = getResources().getString(R.string.per_group_default);
        perGroup = Integer.parseInt(sharedPreferences.getString("per_group", perGroupDefault));
        WordGroup.numberOfWords = perGroup;
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initWordGroupData() {
        WordGroup wordGroup;
        List<Word> wordGroupWords;
        for (int index = 0; index < 400; index += perGroup) {
            wordGroupWords = new ArrayList<>(wordList.subList(index, index + perGroup));
            wordGroup = new WordGroup(index, index + perGroup, wordGroupWords);
            if (wordGroup.getNumberOfMemorized() < perGroup) {
                wordGroupList.add(wordGroup);
            }
            wordGroupListAll.add(wordGroup);
        }
    }

    private void configListView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new WordGroupAdapter(this, wordGroupList, wordGroupListAll);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.word_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_show_all) {
            mAdapter.filter(perGroup, WordGroup.ALL);
            changeTitle(R.string.words_all);
        } else if (id == R.id.action_in_progress) {
            mAdapter.filter(perGroup, WordGroup.IN_PROGRESS);
            changeTitle(R.string.action_in_progress);
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeTitle(int resourceId) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getResources().getString(resourceId));
        }
    }

    @Override
    protected void onRestart() {
        init();
        initWordGroupData();
        configListView();
        super.onRestart();
    }
}
