package com.nurolopher.ieltswords;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.nurolopher.ieltswords.adapter.ViewPagerAdapter;
import com.nurolopher.ieltswords.fragment.ChartFragment;
import com.nurolopher.ieltswords.fragment.ProgressFragment;
import com.nurolopher.ieltswords.fragment.ResultListFragment;

public class StatisticsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        setupActionBar();

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setElevation(0.0f);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Resources resources = getResources();
        adapter.addFragment(new ProgressFragment(), resources.getString(R.string.chart_progress));
        adapter.addFragment(new ChartFragment(), resources.getString(R.string.chart_chart));
        adapter.addFragment(new ResultListFragment(), resources.getString(R.string.chart_table));
        viewPager.setAdapter(adapter);
    }

}
