package com.nurolopher.ieltswords.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nurolopher.ieltswords.ExerciseActivity;
import com.nurolopher.ieltswords.R;
import com.nurolopher.ieltswords.model.WordGroup;
import com.nurolopher.ieltswords.viewholder.WordGroupListViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nurolopher on 11/9/2015.
 */
public class WordGroupAdapter extends RecyclerView.Adapter<WordGroupListViewHolder> {

    private final Context context;
    Resources resources;
    private List<WordGroup> wordGroupsAll;
    private List<WordGroup> wordGroups;
    private int perGroup;

    public WordGroupAdapter(Context context, List<WordGroup> wordGroups, List<WordGroup> wordGroupsAll) {

        this.wordGroups = wordGroups;
        this.wordGroupsAll = wordGroupsAll;
        resources = context.getResources();
        this.context = context;
    }

    @Override
    public WordGroupListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.word_group_list_row, parent, false);
        return new WordGroupListViewHolder(v, new WordGroupListViewHolder.IWordGroupViewHolderClicks() {
            @Override
            public void onClick(View caller) {

                Intent intent = new Intent(context, ExerciseActivity.class);
                View view = caller.findViewById(R.id.progressBar);
                intent.putExtra(ExerciseActivity.EXTRA_WORD_GROUP, (WordGroup) view.getTag());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public void onBindViewHolder(WordGroupListViewHolder holder, int position) {
        final WordGroup wordGroup = wordGroups.get(position);
        String betweenText = wordGroup.getFrom() + " - " + wordGroup.getTo();
        String memorized = wordGroup.getNumberOfMemorized() + "/" + WordGroup.numberOfWords;

        holder.between.setText(betweenText);
        holder.numberOfWordsMemorized.setText(memorized);

        holder.progressBar.setMax(WordGroup.numberOfWords);
        holder.progressBar.setProgress(wordGroup.getNumberOfMemorized());
        holder.progressBar.setTag(wordGroup);
    }

    @Override
    public int getItemCount() {
        return wordGroups.size();
    }

    public void filter(int perGroup, String status) {
        if (status.equals(WordGroup.ALL)) {
            wordGroups = wordGroupsAll;
        } else {
            wordGroups = new ArrayList<>();
            for (WordGroup wordGroup : wordGroupsAll) {
                if (wordGroup.getNumberOfMemorized() < perGroup) {
                    wordGroups.add(wordGroup);
                }
            }
        }
        notifyDataSetChanged();
    }
}
