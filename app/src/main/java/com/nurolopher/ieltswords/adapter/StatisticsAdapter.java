package com.nurolopher.ieltswords.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.nurolopher.ieltswords.R;
import com.nurolopher.ieltswords.model.Statistics;
import com.nurolopher.ieltswords.viewholder.StatisticsViewHolder;

import java.util.ArrayList;

/**
 * Created by nurolopher on 11/9/2015.
 */
public class StatisticsAdapter extends ArrayAdapter<Statistics> {

    private final LayoutInflater mLayoutInflater;

    public StatisticsAdapter(Context context, int resource, ArrayList<Statistics> objects) {
        super(context, resource, objects);

        mLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        StatisticsViewHolder viewHolder;
        if (convertView == null) {
            v = mLayoutInflater.inflate(R.layout.statistics_list_row, parent, false);
            viewHolder = new StatisticsViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (StatisticsViewHolder) v.getTag();
        }
        final Statistics statistics = getItem(position);

        setTexts(viewHolder, statistics);
        return v;
    }

    private void setTexts(StatisticsViewHolder viewHolder, Statistics statistics) {

        viewHolder.date.setText(statistics.getCreatedAtShort());
        viewHolder.correctPercent.setText(String.format("%.2f", statistics.rightInPercent()));
        viewHolder.right.setText(String.format("%d", statistics.getRight()));
        viewHolder.wrong.setText(String.format("%d", statistics.getWrong()));


    }
}
