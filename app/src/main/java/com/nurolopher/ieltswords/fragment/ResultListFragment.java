package com.nurolopher.ieltswords.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nurolopher.ieltswords.R;
import com.nurolopher.ieltswords.adapter.StatisticsAdapter;
import com.nurolopher.ieltswords.model.Statistics;

import java.util.ArrayList;


public class ResultListFragment extends Fragment {

    public ResultListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_result_list, container, false);

        ListView listView = (ListView) view.findViewById(R.id.listViewStatistics);
        ArrayList<Statistics> statistics = (ArrayList<Statistics>) Statistics.find(Statistics.class, null, null, null, "CREATED_AT DESC", null);
        StatisticsAdapter adapter = new StatisticsAdapter(getContext(), R.layout.statistics_list_row, statistics);
        listView.setAdapter(adapter);

        return view;
    }
}