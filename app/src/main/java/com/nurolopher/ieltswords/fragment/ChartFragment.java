package com.nurolopher.ieltswords.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.nurolopher.ieltswords.R;
import com.nurolopher.ieltswords.model.Statistics;

import java.util.ArrayList;
import java.util.List;


public class ChartFragment extends Fragment {

    private LineChart mChart;

    public ChartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chart, container, false);
        mChart = (LineChart) view.findViewById(R.id.chart);
        populateChart();
        return view;
    }

    private void populateChart() {

        ArrayList<String> xValues = new ArrayList<>();
        List<Statistics> statistics = Statistics.find(Statistics.class, null, null, null, "CREATED_AT ASC", null);

        int colorAccent = ContextCompat.getColor(getContext(), R.color.colorAccent);

        ArrayList<Entry> yValues = new ArrayList<>();
        int index = 0;
        for (Statistics st : statistics) {
            xValues.add(st.getCreatedAtShort());
            yValues.add(new Entry(st.rightInPercent(), index));
            index++;
        }

        LineDataSet set1 = new LineDataSet(yValues, getResources().getString(R.string.chat_y_label));
        set1.setFillAlpha(110);

        set1.enableDashedLine(10f, 5f, 0f);
        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(colorAccent);
        set1.setCircleColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        set1.setLineWidth(1f);
        set1.setCircleSize(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setFillAlpha(65);
        set1.setFillColor(colorAccent);
        set1.setDrawFilled(true);

        ArrayList<LineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        LineData data = new LineData(xValues, dataSets);

        mChart.setDescription(getResources().getString(R.string.quiz_chart_description));
        mChart.setDrawBorders(true);
        mChart.setBorderColor(colorAccent);

        mChart.setData(data);
    }
}