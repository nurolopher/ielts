package com.nurolopher.ieltswords.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurolopher.ieltswords.R;

/**
 * Created by nurolopher on 11/11/2015.
 */
public class CompleteListViewHolder extends RecyclerView.ViewHolder {
    public TextView titleView;
    public TextView definitionView;
    public ImageView actionView;
    public TextView statusView;
    public TextView index;

    public CompleteListViewHolder(View base) {
        super(base);
        titleView = (TextView) base.findViewById(R.id.word_title);
        definitionView = (TextView) base.findViewById(R.id.word_definition);
        statusView = (TextView) base.findViewById(R.id.word_status);
        actionView = (ImageView) base.findViewById(R.id.word_action);
        index = (TextView) base.findViewById(R.id.index);
    }
}
