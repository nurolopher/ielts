package com.nurolopher.ieltswords.viewholder;

import android.view.View;
import android.widget.TextView;

import com.nurolopher.ieltswords.R;

/**
 * Created by nurolopher on 11/11/2015.
 */
public class StatisticsViewHolder {
    public TextView date;
    public TextView correctPercent;
    public TextView right;
    public TextView wrong;

    public StatisticsViewHolder(View base) {
        date = (TextView) base.findViewById(R.id.statistics_date);
        correctPercent = (TextView) base.findViewById(R.id.statistics_correct_percent);
        right = (TextView) base.findViewById(R.id.statistics_right);
        wrong = (TextView) base.findViewById(R.id.statistics_wrong);
    }
}
