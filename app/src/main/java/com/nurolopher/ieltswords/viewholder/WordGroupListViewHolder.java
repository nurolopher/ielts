package com.nurolopher.ieltswords.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nurolopher.ieltswords.R;

/**
 * Created by nurolopher on 11/11/2015.
 */
public class WordGroupListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView between;
    public TextView numberOfWordsMemorized;
    public ProgressBar progressBar;
    private IWordGroupViewHolderClicks mListener;

    public WordGroupListViewHolder(View base, IWordGroupViewHolderClicks mListener) {
        super(base);
        between = (TextView) base.findViewById(R.id.between);
        numberOfWordsMemorized = (TextView) base.findViewById(R.id.number_of_words_memorized);
        progressBar = (ProgressBar) base.findViewById(R.id.progressBar);
        base.setOnClickListener(this);
        this.mListener = mListener;
    }

    @Override
    public void onClick(View v) {
        mListener.onClick(v);
    }


    public interface IWordGroupViewHolderClicks {
        void onClick(View caller);
    }
}