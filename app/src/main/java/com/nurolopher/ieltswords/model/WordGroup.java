package com.nurolopher.ieltswords.model;

import android.os.Parcel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nurolopher on 11/9/2015.
 */
public class WordGroup implements Serializable {
    public static final String ALL = "all";
    public static final String IN_PROGRESS = "in_progress";
    private int from;
    private int to;
    public static int numberOfWords;
    private int numberOfMemorized;

    public WordGroup() {
        this.numberOfMemorized = 0;
    }

    public WordGroup(int from, int to, List<Word> words) {
        this();
        this.from = from;
        this.to = to;
        this.process(words);
    }

    private void process(List<Word> words) {
        for (int i = 0, len = words.size(); i < len; i++) {
            if (words.get(i).getStatus() == Word.MEMORIZED) {
                numberOfMemorized++;
            }
        }
    }

    protected WordGroup(Parcel in) {
        from = in.readInt();
        to = in.readInt();
    }


    public int getFrom() {
        return from;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public int getNumberOfMemorized() {
        return numberOfMemorized;
    }

}