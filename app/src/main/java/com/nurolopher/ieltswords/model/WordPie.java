package com.nurolopher.ieltswords.model;

import java.util.List;

/**
 * Created by nurolopher on 11/25/2015.
 */
public class WordPie {

    private int newCount;
    private int memorizedCount;

    public WordPie() {
        newCount = memorizedCount = 0;
        List<Word> words = Word.listAll(Word.class);
        for (int index = 0, len = words.size(); index < len; index++) {
            if (words.get(index).getStatus() == Word.MEMORIZED) {
                memorizedCount++;
            } else {
                newCount++;
            }
        }
    }

    public int getMemorizedCount() {
        return memorizedCount;
    }

    public int getNewCount() {
        return newCount;
    }
}
