package com.nurolopher.ieltswords.model;

import android.content.Context;

import com.nurolopher.ieltswords.R;
import com.nurolopher.ieltswords.utils.Utils;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nurolopher on 11/8/2015.
 */
public class Word extends SugarRecord implements Serializable {

    public static final int NEW = 100;
    public static final int MEMORIZED = 200;
    public static final int ALL = 0;
    private String title;
    private String definition;
    private int status;

    public Word() {

    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Word(String title, String definition) {
        this.title = title;
        this.definition = definition;
        this.status = Word.NEW;
    }

    public String getDefinition() {
        return definition;
    }

    public String getTitle() {
        return title;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static List<Word> findWordsByStatus(int status) {
        List<Word> wordList;
        if (status != Word.ALL)
            wordList = Word.findWithQuery(Word.class, "SELECT * FROM WORD WHERE STATUS = " + status);
        else {
            wordList = Word.listAll(Word.class);
        }
        return wordList;
    }

    public static ArrayList<Word> findBetween(int limit, int offset, boolean all) {
        String query = "SELECT * FROM WORD LIMIT " + limit + " OFFSET " + offset;
        ArrayList<Word> words = (ArrayList<Word>) Word.findWithQuery(Word.class, query);
        for (int index = 0; index < words.size(); index++) {
            if (words.get(index).getStatus() == Word.MEMORIZED && !all) {
                words.remove(index);
                index--;
            }
        }
        return words;
    }

    public static ArrayList<Word> getRandom(int limit) {
        ArrayList<Word> words = (ArrayList<Word>) Word.listAll(Word.class);
        ArrayList<Word> newWords = new ArrayList<>(limit);
        int index;
        Integer[] hashSet = Utils.getRandomHashSet(limit, words.size());
        for (Integer aHashSet : hashSet) {
            index = aHashSet;
            if (index < words.size()) {
                newWords.add(words.get(index));
            }
        }
        return newWords;
    }

    public String getStatusAsString(int currentFilter, Context context) {
        String strStatus = "";
        if (currentFilter != Word.ALL) {
            return "";
        }
        if (status == Word.MEMORIZED) {
            strStatus = context.getResources().getString(R.string.status_memorized);
        } else if (status == Word.NEW) {
            strStatus = context.getResources().getString(R.string.status_new);
        }
        return strStatus;
    }

    public static HashMap<Integer, Word> findRandom(Integer currentIndex) {
        List<Word> words = Word.listAll(Word.class);
        HashMap<Integer, Word> hashMap = new HashMap<>();
        boolean secondRound = false;
        if (currentIndex >= words.size()) {
            currentIndex = 0;
        }
        for (int index = currentIndex, len = words.size(); index < len; index++) {
            Word word = words.get(index);
            if (word.getStatus() == Word.NEW) {
                hashMap.put(index, word);
                return hashMap;
            }
            if (index == len - 1) {
                if (secondRound) {
                    break;
                }
                index = 0;
                secondRound = true;
            }
        }
        return null;
    }
}