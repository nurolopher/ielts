package com.nurolopher.ieltswords.model;

import android.util.Log;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by nurolopher on 11/16/2015.
 */
public class Statistics extends SugarRecord implements Serializable {
    private String createdAt;
    private int wrong;
    private int right;

    public Statistics() {
        this.wrong = 0;
        this.right = 0;
        this.createdAt = (new Date()).toString();
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getWrong() {
        return wrong;
    }

    public void setWrong(int wrong) {
        this.wrong = wrong;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public void increaseRight() {
        right += 1;
        Log.d("statistics right", String.valueOf(right));
    }

    public void increaseWrong() {
        wrong += 1;
        Log.d("statistics wrong", String.valueOf(wrong));
    }

    public float rightInPercent() {
        float tmpWrong = wrong;
        float tmpRight = right;
        float total = tmpWrong + tmpRight;
        if (total > 0) {
            return 100 * tmpRight / total;
        }
        return 0.0f;
    }


    public String getCreatedAtShort() {
        return getCreatedAt().substring(4, 16);
    }
}
