package com.nurolopher.ieltswords;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nurolopher.ieltswords.model.Word;
import com.nurolopher.ieltswords.model.WordGroup;

import java.util.ArrayList;
import java.util.Iterator;

public class ExerciseActivity extends AppCompatActivity implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    public static final String EXTRA_WORD_GROUP = "WORD_GROUP";
    private TextView textViewTitle;
    private TextView textViewDefinition;
    private ArrayList<Word> words;
    private int currentWordIndex;
    private GestureDetectorCompat mDetector;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private WordGroup wordGroup;
    private MenuItem menuItem;
    private ImageButton done;
    private ImageButton next;
    private ImageButton prev;
    private TextView index;
    private int perGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);
        setupActionBar();
        init();
        configProperties();
        updateTextViewData();
        registerOnTouchListener();
        registerButtonListeners();
        initPreferences();

    }

    private void registerButtonListeners() {
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increaseIndex();
                updateTextViewData();
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decreaseIndex();
                updateTextViewData();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeCurrentWord();
                if (currentWordIndex >= words.size() && words.size() > 0) {
                    currentWordIndex = words.size() - 1;
                }
                checkWords();
                updateTextViewData();
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        checkWords();
        return super.onPrepareOptionsMenu(menu);
    }

    private void checkWords() {
        if (words.size() <= 0) {
            if (menuItem != null) {
                menuItem.setVisible(true);
            }
        } else {
            if (menuItem != null) {
                menuItem.setVisible(false);
            }
        }
    }

    private void registerOnTouchListener() {
        View view = findViewById(R.id.relativeLayout);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (words.size() <= 1) {
                    return false;
                }
                mDetector.onTouchEvent(event);
                return true;
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.exercise_menu, menu);
        menuItem = menu.findItem(R.id.restore);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.restore) {
            restoreWords();
            Toast.makeText(this, R.string.restore_successful, Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }


    private void restoreWords() {
        words = Word.findBetween(WordGroup.numberOfWords, wordGroup.getFrom(), true);
        Iterator<Word> iterator = words.iterator();
        while (iterator.hasNext()) {
            Word word = iterator.next();
            word.setStatus(Word.NEW);
            word.save();
        }
        this.currentWordIndex = 0;
        configProperties();
        updateTextViewData();
        checkWords();
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void init() {
        this.currentWordIndex = 0;
        wordGroup = (WordGroup) getIntent().getSerializableExtra(EXTRA_WORD_GROUP);
        words = Word.findBetween(WordGroup.numberOfWords, wordGroup.getFrom(), false);
        mDetector = new GestureDetectorCompat(this, this);
    }

    private void configProperties() {

        textViewTitle = (TextView) findViewById(R.id.word_title);
        textViewDefinition = (TextView) findViewById(R.id.word_definition);
        index = (TextView) findViewById(R.id.index);
        next = (ImageButton) findViewById(R.id.next);
        prev = (ImageButton) findViewById(R.id.prev);
        done = (ImageButton) findViewById(R.id.done);
        mDetector.setOnDoubleTapListener(this);
    }

    private void removeCurrentWord() {
        if (words.size() > 0) {
            Word word = words.get(currentWordIndex);
            if (word != null) {
                word.setStatus(Word.MEMORIZED);
                word.save();
                words.remove(word);
            }
        }

    }


    private void updateTextViewData() {
        if (words.size() < 1 || words.size() <= currentWordIndex || currentWordIndex < 0) {
            noWordLeft();
            return;
        }
        Word word = words.get(currentWordIndex);
        textViewTitle.setText(word.getTitle());
        textViewDefinition.setText(word.getDefinition());

        //update index
        index.setText(String.format("%d / %d", currentWordIndex + 1, words.size()));
    }

    private void noWordLeft() {
        textViewTitle.setText("");
        textViewDefinition.setText(R.string.no_word_left);
        index.setText(String.format("%d/%d", perGroup, perGroup));
    }

    private void decreaseIndex() {
        if (currentWordIndex <= 0) {
            currentWordIndex = words.size();
        }
        currentWordIndex--;
    }

    private void increaseIndex() {
        if (currentWordIndex >= words.size() - 1) {
            currentWordIndex = -1;
        }
        currentWordIndex++;
    }

    private void initPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String perGroupDefault = getResources().getString(R.string.per_group_default);
        perGroup = Integer.parseInt(sharedPreferences.getString("per_group", perGroupDefault));
        WordGroup.numberOfWords = perGroup;
    }

    @Override
    public boolean onDown(MotionEvent event) {
        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        boolean enoughVelocity = Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY;
        boolean rightToLeft, leftToRight;

        if (enoughVelocity) {
            rightToLeft = event1.getX() - event2.getX() > SWIPE_MIN_DISTANCE;
            leftToRight = event2.getX() - event1.getX() > SWIPE_MIN_DISTANCE;
            if (leftToRight) {
                decreaseIndex();
                updateTextViewData();
            } else if (rightToLeft) {
                increaseIndex();
                updateTextViewData();
            }
        }
        return true;
    }

    @Override
    public void onLongPress(MotionEvent event) {
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX,
                            float distanceY) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
        return true;
    }
}
