package xml;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;

import com.nurolopher.ieltswords.R;
import com.nurolopher.ieltswords.model.Word;

import java.util.ArrayList;

/**
 * Implementation of App Widget functionality.
 */
public class WordListAppWidget extends AppWidgetProvider {

    private static final String NEXT_CLICKED = "NEXT_BUTTON_CLICKED";
    private static final String PREV_CLICKED = "PREV_BUTTON_CLICKED";
    private static final String CHECK_CLICKED = "CHECK_BUTTON_CLICKED";
    private static ArrayList<Word> words;
    private static int currentIndex;

    void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                         int appWidgetId) {
        Word word = updateAndGetCurrentWord(context, 1);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.word_list_app_widget);
        views.setTextViewText(R.id.appwidget_title, word.getTitle());
        views.setTextViewText(R.id.appwidget_description, word.getDefinition());

        views.setOnClickPendingIntent(R.id.widget_prev, getPendingSelfIntent(context, WordListAppWidget.PREV_CLICKED));
        views.setOnClickPendingIntent(R.id.widget_next, getPendingSelfIntent(context, WordListAppWidget.NEXT_CLICKED));
        views.setOnClickPendingIntent(R.id.widget_check, getPendingSelfIntent(context, WordListAppWidget.CHECK_CLICKED));

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    private Word updateAndGetCurrentWord(Context context, int amount) {
        if (words == null || words.size() < 1) {
            words = (ArrayList<Word>) Word.findWordsByStatus(Word.NEW);
            if (words == null || words.size() == 0) {
                return getLastMessage(context);
            }
        }
        if (amount == 100) {
            words.get(currentIndex).setStatus(Word.MEMORIZED);
            words.get(currentIndex).save();
            words.remove(currentIndex);
            currentIndex--;
            return updateAndGetCurrentWord(context, 1);
        }
        currentIndex = (Math.abs(currentIndex + amount)) % words.size();
        return words.get(currentIndex);
    }

    private Word getLastMessage(Context context) {
        Word word = new Word();
        word.setTitle(context.getString(R.string.congratulations));
        word.setDefinition(context.getString(R.string.widget_goodbye_message));
        return word;
    }

    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        int amount = 0;
        if (PREV_CLICKED.equals(intent.getAction())) {
            amount = -1;
        } else if (NEXT_CLICKED.equals(intent.getAction())) {
            amount = 1;
        } else if (CHECK_CLICKED.equals(intent.getAction())) {
            amount = 100;
        }

        Word word = updateAndGetCurrentWord(context, amount);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        RemoteViews remoteViews;
        ComponentName watchWidget;

        remoteViews = new RemoteViews(context.getPackageName(), R.layout.word_list_app_widget);
        watchWidget = new ComponentName(context, WordListAppWidget.class);

        remoteViews.setTextViewText(R.id.appwidget_title, word.getTitle());
        remoteViews.setTextViewText(R.id.appwidget_description, word.getDefinition());

        appWidgetManager.updateAppWidget(watchWidget, remoteViews);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
        words = (ArrayList<Word>) Word.findWordsByStatus(Word.NEW);
        currentIndex = 0;
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

